<h2>Local Installation Process</h2>

<ul>
	<li>Install MAMP, XAMPP or WAMP on basis of the system preferences with latest php version.</li>
	<li>Place the project under htdocs folder or www folder depending on the installed local appache server</li>
	<li>From Browser pass the name of your project directory such as: http://localhost/projectfolder  or  http://localhost:8888/projectfolder</li>
</ul>