<?php
namespace Controller;
require "./generate_jwt.php";

class LoginController {

    private $requestMethod;
    private $bodyData;
    private $const;
    public function __construct($requestMethod, $bodyData, $const)
    {
        $this->requestMethod = $requestMethod;
        $this->bodyData = $bodyData;
        $this->const = $const;
    }

    public function processRequest()
    {
        switch ($this->requestMethod) {
            case 'GET':
                $response = $this->logout();
                break;
            case 'POST':
                $response = $this->login($this->bodyData);
                break;
            case 'PUT':
               	echo 'PUT REQUEST';
                break;
            case 'DELETE':
                echo "DELETE REQUEST";
                break;
            default:
                echo 'DEFAULT REQUEST';
                break;
        }
        echo $response;
    }

    private function login($record)
    {
    	try {
            if ($record['email'] == '' &&  $record['password'] == '')
            {
                return json_encode(['status' => false, 'msg' => 'Field data is required']);
            }
            if(!isset($this->const['USERS'][$record['email']])) {
                return json_encode(['status' => false, 'msg' => 'User Not Found']);
            }

            if ($this->const['USERS'][$record['email']]['password'] != $record['password']) {
                return json_encode(['status' => false, 'msg' => 'Invalid Credentials. Please try again.']);
            }
            // Generate JWT Token
            $secret = $this->const['SECRET'];
            $changeLogTime = strtotime(date('Y-m-d h:i:s'));
            $jwtToken = generateToken($record['email'], $secret);
            setcookie("token", $jwtToken, time()+60*60*24*10, '/', null, null, true);
            return json_encode(['status' => true, 'msg' => 'Successfully Logged in']);
		}
		catch(Exception $e) {
			return json_encode(['status' => false, 'msg' => 'Server Error!']);
		}
    }

    private function logout()
    {
        unset($_COOKIE['token']); 
        setcookie('token', null, -1, '/', null, null, true);

        if (!isset($_COOKIE['token'])) {
            return json_encode(['status' => true, 'msg' => 'Successfully Logged out']);
        }
        return json_encode(['status' => false, 'msg' => 'Some error']);
    }
}