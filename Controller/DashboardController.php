<?php
namespace Controller;
require "./generate_jwt.php";

class DashboardController {

    private $requestMethod;
    private $bodyData;
    private $const;
    private $middlewareRecord;

    public function __construct($requestMethod, $bodyData, $const, $middlewareRecord)
    {
        $this->requestMethod = $requestMethod;
        $this->bodyData = $bodyData;
        $this->const = $const;
        $this->middlewareRecord = $middlewareRecord;
    }

    public function processRequest()
    {
        switch ($this->requestMethod) {
            case 'GET':
                echo 'GET REQUEST';
                break;
            case 'POST':
                $response = $this->verifyPassphrase($this->bodyData, $this->middlewareRecord);
                break;
            case 'PUT':
               	echo 'PUT REQUEST';
                break;
            case 'DELETE':
                echo "DELETE REQUEST";
                break;
            default:
                echo 'DEFAULT REQUEST';
                break;
        }
        echo $response;
    }

    private function verifyPassphrase($record, $middlewareRecord)
    {
    	try {
            if ($record['passphrase'] == '') {
                return json_encode(['status' => false, 'msg' => 'Field data is required']);
            }
            /*----------  Check if passpharase is valid  ----------*/
            if (!in_array($record['passphrase'], $this->const['PASSPHRASE'])) {
                return json_encode(['status' => false, 'msg' => 'Please provide valid passpharase']);
            }
            // Generate New Token
            $secret = $this->const['SECRET'];
            $changeLogTime = strtotime(date('Y-m-d h:i:s'));
            unset($_COOKIE['token']); 
            setcookie('token', null, -1);
            $jwtToken = generateToken($middlewareRecord['email'], $secret);
            setcookie("token", $jwtToken, time()+60*60*24*10, '/', null, null, true);
            return json_encode(['status' => true, 'msg' => 'Successfully verified']);
		}
		catch(Exception $e) {
			return json_encode(['status' => false, 'msg' => 'Invoking Function Error']);
		}
    }
}