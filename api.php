<?php
use Controller\LoginController;
use Controller\DashboardController;

require "bootstrap.php";
require "validate_jwt.php";
$const = require "const.php";

header("Access-Control-Allow-Origin: *");
header("Content-Type: application/json; charset=UTF-8");
header("Access-Control-Allow-Methods: OPTIONS,GET,POST,PUT,DELETE");
header("Access-Control-Max-Age: 3600");
header("Access-Control-Allow-Headers: Content-Type, Access-Control-Allow-Headers, Authorization, X-Requested-With");

$uri = parse_url($_SERVER['REQUEST_URI'], PHP_URL_PATH);
$uri = explode( '/api.php/', $uri );

// all of our endpoints start with /person
// everything else results in a 404 Not Found
$requestMethod = $_SERVER["REQUEST_METHOD"];
$bodyData = $_POST;
if (isset($_POST['mehtodCall'])) {
	$functionCall = $_POST['mehtodCall'];
}
$queryData = $_GET;
if (isset($queryData["logout"])) {
	$functionCall = "logout";
}

$token = null;
if (isset($_COOKIE['token'])) {
	$token = $_COOKIE['token'];
}
switch ($functionCall) {
    case 'login':
        $controller = new LoginController($requestMethod, $bodyData, $const);
		$controller->processRequest();
		break;
	case 'verifypassphrase':
		$middlewareRecord = verifyMiddleware($token, $const['SECRET']);
		$controller = new DashboardController($requestMethod, $bodyData, $const, $middlewareRecord);
		$controller->processRequest();
		break;
	case 'verifyToken':
		verifyMiddleware($token, $const['SECRET']);
		echo json_encode(['status' => true]);
		die();
		break;
	case 'logout':
		verifyMiddleware($token, $const['SECRET']);
		$controller = new LoginController($requestMethod, null , $const);
		$controller->processRequest();
		break;
    default:
        break;
}

function verifyMiddleware ($token, $secret) {
	if (!$token) {
		echo json_encode(['status' => false, 'msg' => 'Please Provide Token for Authentication', 'newTokenRequest' => true]);
		die();
	}
	$record = verifyToken($token, $secret);
	if (!$record['status']) {
		echo json_encode($record);
		die();
	} else {
		return $record;
	}
}