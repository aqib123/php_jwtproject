<?php
require "base64urlencode.php";

function verifyToken($jwt, $secret) {
    $tokenParts = explode('.', $jwt);
    $header = base64_decode($tokenParts[0]);
    $payload = base64_decode($tokenParts[1]);
    $record = json_decode($payload, true);
    $signatureProvided = $tokenParts[2];

    // check the expiration time
    $expiration = json_decode($payload)->exp;
    $currentUnixTime = strtotime("now");
    $tokenExpired = ($currentUnixTime > $expiration) ? true : false;
    // var_dump($currentUnixTime);
    // var_dump($expiration);
    // die();

    // build a signature based on the header and payload using the secret
    $base64UrlHeader = base64UrlEncode($header);
    $base64UrlPayload = base64UrlEncode($payload);
    $signature = hash_hmac('sha256', $base64UrlHeader . "." . $base64UrlPayload, $secret, true);
    $base64UrlSignature = base64UrlEncode($signature);

    // verify it matches the signature provided in the token
    $signatureValid = ($base64UrlSignature === $signatureProvided);
    
    if ($tokenExpired) {
        return ['status' => false, 'msg' => 'Your token is expired', 'newTokenRequest' => true];
    } 
    if (!$signatureValid) {
        return ['status' => false, 'msg' => 'Your signature is not valid', 'newTokenRequest' => true];
    }
    return ['status' => true, 'email' => $record['email']];
}

?>